Test-Driven Development with Python
===================================

Web application written in Django(v1.8) framework.

Following the book by Harry J. W. Percival, available at: [Obey the Testing Goat!](http://www.obeythetestinggoat.com/)
