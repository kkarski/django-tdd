Provisioning a new site
=======================

## Required packages:

* Nginx
* Python 3
* Git
* setuptools
* pip
* virtualenv

e.g., on Debian:

    sudo apt-get install nginx git python3 python3-setuptools python3-pip
    sudo pip3 install virtualenv

## Nginx Virtual Host config

* see nginx.template.conf
* replace SITENAME with, e.g., staging.my-domain.com
* replace USERNAME with user account name

## Upstart Job

* see gunicorn-upstart.template.conf
* replace SITENAME with, e.g., staging.my-domain.com
* replace USERNAME with user account name

## Folder structure:
Assume we have a user account at /home/USERNAME

    /home/USERNAME
    └── sites
        └── SITENAME
            ├── database
            ├── source
            ├── static
            └── virtualenv
